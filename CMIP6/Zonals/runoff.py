from scipy.interpolate import griddata
import math
import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
import matplotlib.colors as colors


rho=1000
#------------------------------------------------------------------------------#
path1='/home/kgemmell/weathering/CESM/CESMcontrol/Runoff.nc'
d1=Dataset(path1,'r')
lat=d1.variables['lat'][:]
lon=d1.variables['lon'][:]
lon=np.roll(lon,144)
X, Y =np.meshgrid(lon,lat)

#Define grid spacial resolutions, x1 indicates Hartmann grid
lat1=np.linspace(90,-90,360)
lon1=np.linspace(0,360,720) # CA: BUG FIX
XI,YI=np.meshgrid(lon1,lat1)

#Load in Hartmann model data
ascii_grid = np.loadtxt("/home/kgemmell/weathering/glim_wgs84_0point5deg.txt.asc", skiprows=6)
Land = np.loadtxt("/home/kgemmell/weathering/glim_wgs84_0point5deg.txt.asc", skiprows=6)
Ea= np.loadtxt("/home/kgemmell/weathering/glim_wgs84_0point5deg.txt.asc", skiprows=6)
bsil= np.loadtxt("/home/kgemmell/weathering/glim_wgs84_0point5deg.txt.asc", skiprows=6)
bcarb= np.loadtxt("/home/kgemmell/weathering/glim_wgs84_0point5deg.txt.asc", skiprows=6)

#Removing Oceans from Hartmann data
Land[Land<0]=np.nan
Land[Land>0]=1

#-------------------------Size of Each Gridbox---------------------------------#
r=6371000
A=np.zeros(ascii_grid.shape)
for i in range(0,len(lat1)-1):
  for j in range(0,len(lon1)-1):
    A[i,j]=np.pi/180.*r**2*np.abs(np.sin(lat1[i]*np.pi/180.)-np.sin(lat1[i+1]*np.pi/180.))*np.abs(lon1[j]-lon1[j+1])
A[-1,:]=A[0,:]
A[:,-1]=A[:,0]

A_weighted_global=A/np.sum(A)

A=A*Land #Gives area of Land in each grid box
A2=np.nansum(A) #Gives total area of land
A_weighted=A/A2 #Gives weighting of land in each grid box

#Read in Data
path1='/home/kgemmell/weathering/CESM/CESMcontrol/Runoff.nc'
d1=Dataset(path1,'r')
Runoff1=d1.variables['mrro'][:,:,:]
Run1=np.mean(Runoff1,axis=0)
Run1[np.isnan(Run1)]=0 #CA BUG FIX
Run1[Run1<0]=0


regridR1=griddata((X.flatten(),Y.flatten()), Run1.flatten(), (XI,YI), method='cubic')
regridR1[np.isnan(Land)]=np.nan
regridR1[regridR1<0]=0 #CA BUG FIX

path2='/home/kgemmell/weathering/CESM/2xCO2/Runoff.nc'
d2=Dataset(path2,'r')
Run2=d2.variables['mrro'][:,:,:]
Run2=np.mean(Run2,axis=0)
Run2[np.isnan(Run2)]=0 #CA BUG FIX
regridR2=griddata((X.flatten(),Y.flatten()), Run2.flatten(), (XI,YI), method='cubic')
regridR2[np.isnan(Land)]=np.nan
regridR2[regridR2<0]=0 #CA BUG FIX

path4='/home/kgemmell/weathering/CESM/4xCO2/Runoff.nc'
d4=Dataset(path4,'r')
Run4=d4.variables['mrro'][:,:,:]
Run4=np.mean(Run4,axis=0)
Run4[np.isnan(Run4)]=0 #CA BUG FIX
regridR4=griddata((X.flatten(),Y.flatten()), Run4.flatten(), (XI,YI), method='cubic')
regridR4[np.isnan(Land)]=np.nan
regridR4[regridR4<0]=0 #CA BUG FIX

R1=(regridR1/rho)*60*60*24*365.25*1000  #mm/a
R2=(regridR2/rho)*60*60*24*365.25*1000  #mm/a
R4=(regridR4/rho)*60*60*24*365.25*1000  #mm/a


tot1x=R1*A/1000 #m^3/a
tot2x=R2*A/1000 #m^3/a
tot4x=R4*A/1000 #m^3/a



#Multiply by area, and then sum
zonalR1=np.nansum(tot1x,axis=1)
zonalR2=np.nansum(tot2x,axis=1)
zonalR4=np.nansum(tot4x,axis=1)
#------------------------------------------------------------------------------#
#Plotting the Data
plt.plot(lat1,zonalR1,'k',label='280')
plt.plot(lat1,zonalR2,label='560')
plt.plot(lat1,zonalR4,label='1120')

plt.legend(title='$CO_2$ Concentration (ppm)', loc='upper right')

plt.ylabel('Runoff ($m^3$ yr$^{-1}$)',fontsize=14)
plt.xlabel('Latitude',fontsize=14)
#plt.title('Total Zonal Runoff for CESM Model Runs',fontsize=16)

plt.show()
plt.savefig('ZonalRunoff.png')

