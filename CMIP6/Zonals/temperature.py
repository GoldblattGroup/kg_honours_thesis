from scipy.interpolate import griddata
import math
import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
import matplotlib.colors as colors
#------------------------------------------------------------------------------#
#Read in Data
path1='/home/kgemmell/weathering/CESM/CESMcontrol/TS.nc'
d1=Dataset(path1,'r')
lat=d1.variables['lat'][:]
lon=d1.variables['lon'][:]
lon=np.roll(lon,144)
TS1=d1.variables['ts'][:,:,:]
TS1=np.nanmean(TS1,axis=0)

path2='/home/kgemmell/weathering/CESM/2xCO2/TS.nc'
d2=Dataset(path2,'r')
TS2=d2.variables['ts'][:,:,:]
TS2=np.nanmean(TS2,axis=0)

path4='/home/kgemmell/weathering/CESM/4xCO2/TS.nc'
d4=Dataset(path4,'r')
TS4=d4.variables['ts'][:,:,:]
TS4=np.nanmean(TS4,axis=0)

#Define grid spacial resolutions, x1 indicates Hartmann grid
X, Y =np.meshgrid(lon,lat)
lat1=np.linspace(90,-90,360)
lon1=np.linspace(0,360,720)
XI,YI=np.meshgrid(lon1,lat1)

#Load in Hartmann model data
ascii_grid = np.loadtxt("glim_wgs84_0point5deg.txt.asc", skiprows=6)
Land = np.loadtxt("glim_wgs84_0point5deg.txt.asc", skiprows=6)
Land[Land<0]=np.nan
Land[Land>0]=1

regridT1=griddata((X.flatten(),Y.flatten()), TS1.flatten(), (XI,YI), method='cubic')
regridT1[np.isnan(Land)]=np.nan

regridT2=griddata((X.flatten(),Y.flatten()), TS2.flatten(), (XI,YI), method='cubic')
regridT2[np.isnan(Land)]=np.nan

regridT4=griddata((X.flatten(),Y.flatten()), TS4.flatten(), (XI,YI), method='cubic')
regridT4[np.isnan(Land)]=np.nan


zonalT1=np.nanmean(regridT1,axis=1)
zonalT2=np.nanmean(regridT2,axis=1)
zonalT4=np.nanmean(regridT4,axis=1)

#------------------------------------------------------------------------------#
#Plotting the Data
plt.plot(lat1,zonalT1,'k',label='280')
plt.plot(lat1,zonalT2,label='560')
plt.plot(lat1,zonalT4,label='1120')

plt.legend(title='$CO_2$ Concentration (ppm)', loc='upper right')

plt.ylabel('Temperature (K)',fontsize=14)
plt.xlabel('Latitude',fontsize=14)
#plt.title('Zonally Averaged Temperature Over Land for CMIP-6 Model Runs',fontsize=16)

plt.show()
plt.savefig('ZonalTemp.png')

